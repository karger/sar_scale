# This code is a first research grade try of fitting a parameterized SAR model
# to a timeseries of changing PFT areas (here taken from LPJ-Guess).
# The plot data for parametrization is taken from sPlot-Open
# the environmental data is taken from CHELSA V2.1
# Z: is /lud11


# read in the libraries
library(ncdf4)
library(raster)
library(COMPoissonReg)
library(segmented)
library(scales)

# use some example data from LPJ-GUESS
nc1 <- nc_open('Z:/karger/LPJ-GUESS/lpj-guess_gswp3_nobc_hist_varsoc_co2_pft_bine_global_annual_1971_2010.nc')

# load in the boral forests pfts
st1_bine <- stack('Z:/karger/LPJ-GUESS/lpj-guess_gswp3_nobc_hist_varsoc_co2_pft_bine_global_annual_1971_2010.nc') #Boreal shade intolerant needleleaved evergreen
st1_bne <- stack('Z:/karger/LPJ-GUESS/lpj-guess_gswp3_nobc_hist_varsoc_co2_pft_bne_global_annual_1971_2010.nc') #Boreal needleleaved evergreen
st1_bns <- stack('Z:/karger/LPJ-GUESS/lpj-guess_gswp3_nobc_hist_varsoc_co2_pft_bns_global_annual_1971_2010.nc') #Boreal needleleved summergreen

# load in the environmental data
bio1 <- raster("C:/Users/Administrator/Downloads/CHELSA_bio1_1981-2010_V.2.1.tif")
bio12 <- raster("C:/Users/Administrator/Downloads/CHELSA_bio12_1981-2010_V.2.1.tif")

# combine
st1 <- st1_bine + st1_bne + st1_bns #calc(stack(st1_bine, st1_bne, st1_bns), sum)

# reproject to equal area
proj4st <- '+proj=cea +lon_0=0 +lat_ts=0 +x_0=0 +y_0=0 +ellps=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs'
st1_prj <- projectRaster(st1, crs=crs(proj4st))

# create PFT extent
pftext <- st1_prj[[1]]
pftext[pftext>0]<-1

# get a specific continent
conts <- shapefile('Z:/karger/scareb/continent-poly/North America.shp')
conts_prj <- spTransform(conts, crs(pftext))
conts_rs <- rasterize(conts_prj, pftext)

# PFTt continental extent
pftext_cont <- pftext
pftext_cont[is.na(conts_rs)]<-NA

# read in the plot data for parametrization
path <- "Z:/karger/scareb/"
splot <- "Z:/karger/splot_open/"

sp <- read.csv(paste0(splot, '3474_54_sPlotOpen_DT.txt'), sep='\t')
header <- read.csv(paste0(splot, '3474_54_sPlotOpen_header.txt'), sep='\t')

# example biome
df1 <- header[header$Continent=='North America',]
df1 <- df1[df1$Biome=='Boreal zone',]
df1 <- df1[df1$Forest==TRUE,]
df1 <- df1[!is.na(df1$PlotObservationID),]
coordinates(df1)<-~Longitude+Latitude
crs(df1)<-crs(conts)

# extract environmental data
df1$bio1 <- extract(bio1, df1)
df1$bio12 <- extract(bio12, df1)

df1 <- spTransform(df1, crs(pftext))
df1$clip_extent <- extract(pftext_cont, df1)
df1 <- as.data.frame(df1)
df1 <- df1[df1$clip_extent==1,]

sp1 <- sp[sp$PlotObservationID%in%df1$PlotObservationID,]

# sample the biome
df_samp <- data.frame(a=double(), sr=integer(), it=integer(), henv1=double(), henv2=double(), bio1=double(), bio12=double(), continent=character(), biome=character(), np = integer())

# create the species area curves by random sampling plots and combining them
for (n in 1:10)
{
  samplevect <- c(seq(2,500,2),seq(501,4000,100))
  for (m in samplevect)
  {
    print(c(n,m))
    # sample random plots
    samp <- df1[sample(nrow(df1), m, replace = TRUE), ]
    # get the species list for all plots in the random rows
    sp_list <- sp1[sp1$PlotObservationID%in%samp$PlotObservationID,]
    # calculate species richness and area
    sr <- length(unique(sp_list$Species))
    a <- sum(samp$Releve_area)
    it <- m
    # euclidean distances between environment
    henv1 <- mean(dist(as.data.frame(samp$bio1), method = 'euclidean' ))
    henv2 <- mean(dist(as.data.frame(samp$bio12), method = 'euclidean' ))
    df_samp <- rbind(df_samp, data.frame(a=a, sr=sr, it=it, henv1=henv1, henv2=henv2, bio1=samp$bio1, bio12=samp$bio12, continent=samp$Continent, biome=samp$Biome, np=m))
  }
}

# clean the data for NAs and save
df_ana <- df_samp[complete.cases(df_samp),]
write.table(df_ana,'Z:/karger/scareb/df_ana_2.txt')
df_ana <- read.table('Z:/karger/scareb/df_ana_2.txt', header=T)

# log transform area
df_ana$log_a <- log(df_ana$a)

df_ana_org <- df_ana
df_ana <- df_ana[df_ana$a <= 40000,]

scale_predictors <- TRUE

# TRY - scale the predictors
if (scale_predictors==TRUE)
{
df_ana$henv1 <- scale(df_ana$henv1)
df_ana$henv1_2 <- scale(df_ana$henv1^2)

df_ana$henv2 <- scale(df_ana$henv2)
df_ana$henv2_2 <- scale(df_ana$henv2^2)
}

# TRY - do not scale the predictors
if (scale_predictors==FALSE)
{
df_ana$henv1_2 <- df_ana$henv1^2
df_ana$henv2_2 <- df_ana$henv2^2
}

# set the options for the non-linear parameter approxmiation
options(COMPoissonReg.optim.method = 'L-BFGS-B')
options(COMPoissonReg.optim.control = list(maxit = 150))

# fit a COMP model

df_ana <- df_ana[complete.cases(df_ana), ]

# fit a full model
runfull = TRUE
if (runfull == TRUE)
{
m_full <-  glm.cmp(formula.lambda = sr ~ log_a + henv1 + henv2,
              formula.nu = sr ~ 1,
              beta.init =  c(-3, 0.00001, 0.00001, 0.00001),
              gamma.init = c(0.7),
              data = df_ana)
}
saveRDS(m_full, 'Z:/karger/scareb/m_full.RDS')
m_full <- readRDS('Z:/karger/scareb/m_full.RDS')

# fit a split model
if (runfull == FALSE)
{
# implement split model
fit <- lm(sr~a, data=df_ana)
segmented.fit <- segmented(fit, seg.Z = ~a, psi=max(df_ana$a)/2)

breakpoint <- summary(segmented.fit)$psi[2]

df_ana_seg1 <- df_ana[df_ana$a<=breakpoint,]
df_ana_seg2 <- df_ana[df_ana$a>breakpoint,]

# split COMP reg
m_seg1 <-  glm.cmp(formula.lambda = sr ~ log_a + henv1 + henv2,
                   formula.nu = sr ~ 1,
                   beta.init =  c(-3, 0.00001, 0.00001, 0.00001),
                   gamma.init = c(0.7),
                   data = df_ana_seg1)

m_seg2 <-  glm.cmp(formula.lambda = sr ~ log_a + henv1 + henv2,
                   formula.nu = sr ~ 1,
                   beta.init =  c(-5, .0000000000001, .0000000000001, .0000000000001),
                   gamma.init = c(0.7),
                   data = df_ana_seg2)
}


# function for predictions of a COMP model
f_CMP_pred <- function(m, dpred)
{
  nu <- exp(m$gamma)
  coef <- m$beta
  lambda <-  exp(
    coef["X:(Intercept)"] +
      coef["X:log_a"] * dpred$log_a +
      coef["X:henv1"] * dpred$henv1 +
      #coef["X:henv1_2"] * dpred$henv1_2 +
      coef["X:henv2"] * dpred$henv2)# +
      #coef["X:henv2_2"] * dpred$henv2_2)

  dpred$SR_pred <- (lambda^(1/nu)) - ((nu-1)/(2*nu))

  return(dpred)
}


# plot the prediction on example data
dpred <- expand.grid(a=seq(10,2500000, 1000), henv1=0, henv2=0)
dpred$log_a = log(dpred$a)
dpred <- f_CMP_pred(m_full, dpred)

#pdf ('D:/bio/Bio_Backup/Proposals/SNF_consolidator/figures/fig_1_prop.pdf', height=5, width = 15)
par(mfrow = c(1,4))

# plot the results with variations in henv2
#Create a function to generate a continuous color palette
rbPal <- colorRampPalette(c('#ca0020',
  '#f4a582',
  '#f7f7f7',
  '#92c5de',
  '#0571b0'))

#This adds a column of color values
# based on the y values
df_ana$Col <- rbPal(10)[as.numeric(cut(df_ana$henv2,breaks=11))]

plot(df_ana$a, df_ana$sr, pch=19, col=alpha('black',0.75),cex=0.25,
     ylab="species richness", xlab="area [sqr. m]")

#plot(dpred$a, dpred$SR_pred)
points(dpred$a, dpred$SR_pred, "l", col="grey50")

dpred <- expand.grid(a=seq(10,2500000, 1000), henv1=0, henv2=-50)
dpred$log_a = log(dpred$a)

dpred <- f_CMP_pred(m_full, dpred)
points(dpred$a, dpred$SR_pred, "l", col="blue")

dpred <- expand.grid(a=seq(10,2500000, 1000), henv1=0, henv2=50)
dpred$log_a = log(dpred$a)

dpred <- f_CMP_pred(m_full, dpred)
points(dpred$a, dpred$SR_pred, "l", col="red")


# create a raster stack of the predictors
# create the extent
ext1 <- extent(-120,-100,40,50)

# create the predictor layers
bio1_c <- crop(bio1, ext1)
#bio1_2_c <- bio1_c
#values(bio1_2_c)<-values(bio1_2_c)^2

bio12_c <- crop(bio12, ext1)
#bio12_2_c <- bio12_c
#values(bio12_2_c)<-values(bio12_2_c)^2

# create enviromental heterogeneity
fun1 <- function(x, ...){mean(dist(as.data.frame(na.omit(x)), method = 'euclidean' ))}

bio1_c_agg <- aggregate(bio1_c, fact=10, expand=F,fun=fun1)
bio12_c_agg <- aggregate(bio12_c, fact=10, expand=F,fun=fun1)
area_c <- bio1_c_agg
values(area_c)<-log(400)

#pdf ('Z:/karger/scareb/fig_2.pdf', height=5, width = 10)

plot(bio1_c_agg, col=viridis(10), main = 'henv1')
#plot(bio12_c_agg, col=viridis(10),  main = 'henv2')
#dev.off()
## scale environmental data

# CHECK SCALING BY BACKTRANSFORM
m <- m_full
bio1_scaled <- scale(bio1_c_agg, center = attr(df_ana_org$henv1,'scaled:center'),scale = attr(df_ana$henv1,'scaled:scale'))
bio12_scaled <- scale(bio12_c_agg, center = attr(df_ana$henv2,'scaled:center'), scale =  attr(df_ana$henv2,'scaled:scale'))

## this or this:
#bio1_scaled <- scale(bio1_c, center = mean(df_ana$henv1),scale = sd(df_ana$henv1))
#bio12_scaled <- scale(bio12_c, center = mean(df_ana$henv2), scale = sd(df_ana$henv2))


################################################################################

# spatial prediction of the model
bio1_2_scaled <- bio1_scaled^2
bio12_2_scaled <- bio12_scaled^2

# stack and rename
pred <- stack(area_c, bio1_scaled, bio12_scaled, bio1_2_scaled, bio12_2_scaled)
pred <- stack(area_c, bio1_c_agg, bio12_c_agg)
names(pred)<-c('log_a','henv1','henv2')#,'henv1_2','henv2_2')

# create the parameter grids
# nu parameter
nu <- area_c
values(nu) <- exp(m$gamma)

#lambda parameter
coef1 <- m$beta

calc_lambda <- function(..., coef)
{
exp(coef["X:(Intercept)"] +
    coef["X:log_a"] * ...[1] +
    coef["X:henv1"] * ...[2] +
    coef["X:henv2"] * ...[3]
    )
}


lambda <- calc(pred, fun = function(x){calc_lambda(x, coef=coef1)})
#pdf ('Z:/karger/scareb/fig_3.pdf', height=5, width = 5)
#plot(lambda, col=magma(10))
#dev.off()
writeRaster(lambda,'C:/Users/karger/Documents/lambda.tif', format="GTiff", overwrite=T)
# create coefficients stack
st_coeff <- stack(lambda, nu)

pred_COMP <- function(x){
  sr = NA
  try(sr<-(x[1]^(1/x[2])) - ((x[2]-1)/(2*x[2])))
  return(sr)
}

library(viridis)
sr_scaled <- calc(st_coeff, pred_COMP)
sr1_scaled <- calc(st_coeff, pred_COMP)
#pdf ('Z:/karger/scareb/fig_4.pdf', height=5, width = 5)
plot(sr_scaled, col=viridis(10), main="SR")
#dev.off()
writeRaster(sr_scaled,'C:/Users/karger/Documents/sr_400.tif', format="GTiff", overwrite=T)
#sr_scaled <- raster('C:/Users/karger/Documents/sr_400.tif')
# validate this shit
df1_test <- header[header$Continent=='North America',]
df1_test <- df1_test[df1_test$Biome=='Boreal zone',]
df1_test <- df1_test[df1_test$Forest==TRUE,]
df1_test <- df1_test[!is.na(df1_test$PlotObservationID),]
df1_test <- df1_test[df1_test$Releve_area==400,]
coordinates(df1_test)<-~Longitude+Latitude


df1_test <- crop(df1_test, ext1)
points(df1_test, col="red")
df1_test$mod <- extract(sr_scaled, df1_test)
head(df1_test)

# create species richness for validation
df_test_ana <- data.frame(obs=double(), mod=double())

for (n in unique(df1_test$PlotObservationID))
{
sp1 <- df1_test[df1_test$PlotObservationID==n,]
sp_list <- sp[sp$PlotObservationID%in%sp1$PlotObservationID,]
# calculate species richness and area
obs <- length(unique(sp_list$Species))
mod <- df1_test[df1_test$PlotObservationID==n,]$mod
df_test_ana <- rbind(df_test_ana, data.frame(obs=obs, mod=mod))
}

#apply this to a timeseries
m = m_full
nu <- exp(m$gamma)

nu_prj <- st1_prj[[1]]
lambda_prj <- st1_prj[[1]]
henv1_prj <- st1_prj[[1]]
henv2_prj <- st1_prj[[1]]

values(nu_prj) <- nu
values(lambda_prj) <- mean(values(lambda))
values(henv1_prj) <- 0
values(henv2_prj) <- 0

nu <- exp(m$gamma)
coef1 <- m$beta

# loop over the LPJ timeseries
for(n in c(1,40))
{
  a <- st1_prj[[n]]
  a <- a*50000*50000
  a <- log(a)
  str_ana <- stack(a, henv1_prj, henv2_prj)
  lambda1 <- calc(str_ana, fun = function(x){calc_lambda(x, coef=coef1)})
  st_coeff <- stack(lambda1, nu_prj)
  sr_scaled <- calc(st_coeff, pred_COMP)
  assign(paste0('lpj_sr_',n),sr_scaled)
}


# plot the results for the proposal
luc <- raster("D:/bio/Bio_Backup/Proposals/SNF_consolidator/consensus_full_class_1.tif")
luc_c <- crop(luc, extent(sr1_scaled))
luc_c <- aggregate(luc_c, 10)
values(luc_c)[values(luc_c>0)]<-1

sr1_scaled_boral <- sr1_scaled*luc_c

pdf ('D:/bio/Bio_Backup/Proposals/SNF_consolidator/figures/fig_1_prop1.pdf', height=3, width = 9)
par(mfrow = c(1,3), oma=c(1,1,1,1))
# plot the results with variations in henv2
#Create a function to generate a continuous color palette
rbPal <- colorRampPalette(c('#ca0020',
                            '#f4a582',
                            '#f7f7f7',
                            '#92c5de',
                            '#0571b0'))

#This adds a column of color values
# based on the y values
df_ana$Col <- rbPal(10)[as.numeric(cut(df_ana$henv2,breaks=11))]

plot(df_ana$a, df_ana$sr, pch=19, col=alpha('black',0.75),cex=0.25,
     ylab="species richness", xlab="area [sqr. m]")
#plot(dpred$a, dpred$SR_pred)
points(dpred$a, dpred$SR_pred, "l", col="grey50")


dpred <- expand.grid(a=seq(10,2500000, 1000), henv1=0, henv2=-50)
dpred$log_a = log(dpred$a)

dpred <- f_CMP_pred(m_full, dpred)
points(dpred$a, dpred$SR_pred, "l", col="blue")

dpred <- expand.grid(a=seq(10,2500000, 1000), henv1=0, henv2=50)
dpred$log_a = log(dpred$a)

dpred <- f_CMP_pred(m_full, dpred)
points(dpred$a, dpred$SR_pred, "l", col="red")


plot(bio1_c_agg, col=viridis(10), main = 'env. heterog.', ylab="latitude", xlab="longitude")

df1 <- header[header$Continent=='North America',]
df1 <- df1[df1$Biome=='Boreal zone',]
df1 <- df1[df1$Forest==TRUE,]
df1 <- df1[!is.na(df1$PlotObservationID),]
coordinates(df1)<-~Longitude+Latitude
points(df1, col="black",pch=19, cex=1)
points(df1, col="red",pch=19, cex=0.5)

plot(sr1_scaled_boral, col=plasma(10), main="SR", ylab="latitude", xlab="longitude")

dev.off()


diff_sr <- (100*(lpj_sr_1-lpj_sr_40)+0.001)/(lpj_sr_1+0.001)
writeRaster(diff_sr, file="D:/bio/Bio_Backup/Proposals/SNF_consolidator/figures/diff_sr.tif", format="GTiff")
ext2 <- extent(-16e+06,-5e+06,3e+06,7e+06)

#diff_sr_prj <- projectRaster(diff_sr, crs=crs(st1_bine))
library('RColorBrewer')
cols <- brewer.pal(n = 11, name = 'RdBu')
diff_sr_c <- crop(diff_sr, ext2)

countries <- shapefile('D:/bio/Bio_Backup/Proposals/SNF_consolidator/figures/countries/ne_50m_admin_0_countries.shp')
cc <- spTransform(countries, crs(pftext))

countries_c <- crop(cc, extent(diff_sr_c))

pdf ('D:/bio/Bio_Backup/Proposals/SNF_consolidator/figures/fig_2_prop_LPJ.pdf', height=10, width = 16)
{
  plot(diff_sr_c, breaks=seq(-10,10,2), col=cols, box=F)
  plot(countries_c, add=T)
  par(xpd=NA)
  cords<-c(-1.4e+07,-8.0e+06,2.5e+06,2.75e+06)
  tg1 = 0.5
  lb1 = 1
  lg1 = 1.75
  cx1 = 1
  try(cscl(rev(cols),cords,zrng=c(-10,10),at = c(),tria = "b", horiz = T, lablag=1, cx=cx1))

  cscl(rev(cols[2:(length(cols)-1)]),cords,zrng=c(-10,10),at = seq(-10,10,by=20/9),tria = "n", horiz = T,
       labs=c("<-10    ","-8","-6",  "-4",    "-2",   "2", "4", "6", "8", ">10"),  cx=cx1,
       title="plant species richness change 1971-2010 [%]",titlag=lg1,tickle=tg1,lablag=lb1)
  par(xpd=TRUE)

}
dev.off()

for(n in 1:40)
{
png(paste0('Z:/karger/scareb/lpj_',n,'.png'))
plot(get(paste0('lpj_sr_',n)),col=viridis(11), breaks=seq(0,60000,6000))
dev.off()
}

